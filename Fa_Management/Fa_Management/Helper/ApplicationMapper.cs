﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;

namespace FA_Management.Helper
{
    public class ApplicationMapper:Profile
    {
        public ApplicationMapper()
        {
            CreateMap<Syllabus, SyllabusModel>().ReverseMap();
            CreateMap<Syllabus, SyllabusModelUpdate>().ReverseMap();
            CreateMap<Syllabus, SyllabusCVS>().ReverseMap();
            CreateMap<LearningMaterial, MaterialModel>().ReverseMap();
			CreateMap<TrainingProgram, TrainingProgramModel>().ReverseMap();
			CreateMap<TrainingProgram, TrainingProgramModelCreate>().ReverseMap();
			CreateMap<TrainingProgram, TrainingProgramModelUpdate>().ReverseMap();
            CreateMap<TrainingProgram, TemplateTrainingProgram>().ReverseMap();
			CreateMap<Class, ClassModel>().ReverseMap();
			CreateMap<Class, ClassModelCreate>().ReverseMap();
			//CreateMap<ContentClass, ContentClassModel>().ReverseMap();
            CreateMap<DetailClass,DetailClassModel>().ReverseMap(); 
			CreateMap<Calendar, CalendarModel>().ReverseMap();
			CreateMap<ContentProgram,ContentProgramModel>().ReverseMap();
		}
	}
}
