﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA_Management.Data
{
    public class DetailClass
    {
        [Key]
        public int Id { get; set; }

        [ForeignKey("Class")]
        public int Class_id { get; set; }
        public Class Class { get; set; }

        [ForeignKey("TrainingProgram")]
        public int Progam_id { get; set; }  
        public TrainingProgram TrainingProgram { get; set; }

        [ForeignKey("Syllabus")]
        public int Syllabus_id { get; set; }
        public Syllabus Syllabus { get; set; }

        public int Status { get; set; }    
    }
}
