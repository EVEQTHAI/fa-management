﻿using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA_Management.Data
{
    public class LearningMaterial
    {
        [Key]
        public int Material_id { get; set; }
        public string File_name { get; set; }
        public string Content_type { get; set; }
        public long? File_size { get; set; }
        public string Upload_by { get; set; }
        public DateTime Upload_date { get; set; }
        public bool Status { get;set; }
    }
}
