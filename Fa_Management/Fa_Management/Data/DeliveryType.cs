﻿using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class DeliveryType
    {
        [Key]
        public int Delivery_id { get; set; }
        public string Delivery_name { get; set; }
    }
}
