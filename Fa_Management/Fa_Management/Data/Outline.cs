﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class Outline
    {
        [Key]
        public int Outline_id { get; set; }
        public int Day_no { get; set; }
        [ForeignKey("Unit")]
        public int Unit_id { get; set; }
        public Unit Unit { get; set; }
    }
}
