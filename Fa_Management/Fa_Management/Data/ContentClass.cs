﻿using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA_Management.Data
{
    public class ContentClass
    {
        [Key]
        public int Content_class_id { get; set; }
        public int Status { get; set; }
        [ForeignKey("IdentityUser")]
        public string User_id { get; set; }
        public IdentityUser IdentityUser { get; set; }
        [ForeignKey("Class")]
        public int Class_id { get; set; }
        public Class Class { get; set; }
    }
}
