﻿using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class DeliveryPrinciple
    {
        [Key]
        public int Principle_id { get; set; }
        public string Training { get; set; }
        public string Retest { get; set; }
        public string Marking { get; set; }
        public string Waiver_criteria { get; set; }
        public string Other { get; set; }
    }
}
