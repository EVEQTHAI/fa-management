﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class Calendar
    {
        [Key]
        public int Calendar_id { get; set; }
        public DateTime Assigned_datetime { get; set; }

        [ForeignKey("Class")]
        public int Class_id { get; set; }
        public Class Class { get; set; }
    }
}
