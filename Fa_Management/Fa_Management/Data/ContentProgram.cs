﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA_Management.Data
{
    public class ContentProgram
    {
        [Key]
        public int Content_program_id { get; set; }
        public bool Status { get; set; }
        [ForeignKey("Syllabus")]
        public int Syllabus_id { get; set; }
        public Syllabus Syllabus { get; set; }
        [ForeignKey("TrainingProgram")]
        public int Program_id { get; set; }
        public TrainingProgram TrainingProgram { get; set; }
    }
}
