﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace FA_Management.Data
{
    public class ApplicationDbContext: IdentityDbContext<IdentityUser>
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext>opts):base(opts) { }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.ApplyConfiguration(new ApplicationUserEntityConfiguration());
        }
        public DbSet<AssessmentScheme> AssessmentScheme { get; set; }
        public DbSet<Class> Class { get; set; }
        public DbSet<DeliveryPrinciple> DeliveryPrinciple { get; set; }
        public DbSet<DeliveryType> DeliveryType { get; set; }
        public DbSet<Outline> Outline { get; set; }
        public DbSet<OutputStandard> OutputStandard { get; set; }
        public DbSet<Syllabus> Syllabus { get; set; }
        public DbSet<ContentProgram> ContentProgram { get; set; }
        public DbSet<ContentClass> ContentClass { get; set; }
        public DbSet<TrainingProgram> TrainingProgram { get; set; }
        public DbSet<Unit> Unit { get; set; }
        public DbSet<Calendar> Calendar { get; set; }
        public DbSet<LearningMaterial> LearningMaterial { get; set; }
        public DbSet<DetailClass> DetailClass { get; set; }

        private class ApplicationUserEntityConfiguration : IEntityTypeConfiguration<ApplicationUser>
        {
            public void Configure(EntityTypeBuilder<ApplicationUser> builder)
            {
                builder.Property(u => u.FirstName);
                builder.Property(u => u.LastName);
                builder.Property(u => u.Birthday);
                builder.Property(u => u.Gender);
                builder.Property(u => u.Base_salary);
                builder.Property(u => u.Status);
            }
        }
    }
}
