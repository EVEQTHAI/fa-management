﻿using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class AssessmentScheme
    {
        [Key]
        public int AS_id { get; set; }
        public int Quiz { get; set; }
        public int Assignment { get; set; }
        public int Final { get; set; }
        public int Final_theory { get; set; }
        public int Final_Pactice { get; set; }
        public int Passing_gpa { get; set; }
    }
}
