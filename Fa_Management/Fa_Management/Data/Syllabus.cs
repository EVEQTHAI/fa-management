﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class Syllabus
    {
        [Key]
        public int Syllabus_id { get; set; }
        public string Syllabus_name { get; set; }
        public DateTime Create_on { get; set; }
        public string Create_by { get; set; }
        public int Duration { get; set; }
        public string Level { get; set; }
        public int Attendee_number { get; set; }
        public string Technical_requirement { get; set; }
        public string Objective_source { get; set; }
        [Required]
        public string Code { get; set; }
        [NotMapped]
        public int Version_i1 { get; set; }
        [NotMapped]
        public int Version_i2 { get; set; }
        [NotMapped]
        public int Version_i3 { get; set; }

        public string Version
        {
            get
            {
                return $"{Version_i1}.{Version_i2}.{Version_i3}";
            }
            set
            {
                var parts = value.Split('.');
                if (parts.Length >= 3 &&
                    int.TryParse(parts[0], out int i1) &&
                    int.TryParse(parts[1], out int i2) &&
                    int.TryParse(parts[2], out int i3))
                {
                    Version_i1 = i1;
                    Version_i2 = i2;
                    Version_i3 = i3;
                }
                else
                {
                    // Handle invalid version string format
                }
            }
        }
        public int Status { get; set; }
        [ForeignKey("Outline")]
        public int Outline_id { get; set; }
        public Outline Outline { get; set; }
        [ForeignKey("AssessmentScheme")]
        public int AS_id { get; set; }
        public AssessmentScheme AssessmentScheme { get; set; }
        [ForeignKey("OutputStandard")]
        public int Output_id { get; set; }
        public OutputStandard OutputStandard { get; set; }
        [ForeignKey("DeliveryPrinciple")]
        public int Principle_id { get; set; }
        public DeliveryPrinciple DeliveryPrinciple { get; set; }
    }
}
