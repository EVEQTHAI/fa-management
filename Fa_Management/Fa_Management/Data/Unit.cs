﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class Unit
    {
        [Key]
        public int Unit_id { get; set; }
        public String Unit_name { get; set; }
        public string Output_standard { get; set; }
        public int Training_time { get; set; }
        public string Method { get; set; }
        [ForeignKey("DeliveryType")]
        public int Delivery_id { get; set; }
        public DeliveryType DeliveryType { get; set; }
        [ForeignKey("LearningMaterial")]
        public int Material_id { get; set; }
        public LearningMaterial LearningMaterial { get; set;}
    }
}
