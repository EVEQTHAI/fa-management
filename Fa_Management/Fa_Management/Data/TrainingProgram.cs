﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class TrainingProgram
    {
        [Key]
        public int Program_id { get; set; }
        public string Program_name { get; set; }
        public string Information { get; set; }
        public DateTime Create_on { get; set; }
        public string Create_by { get; set; }
        public int Status { get; set; }
        public int Duration { get; set; }
        //{
        //    get
        //    {
        //        if (Syllabus != null && Syllabus.TrainingContents != null && Syllabus.TrainingContents.Any())
        //        {
        //            return Syllabus.TrainingContents.Sum(content => content.Duration);
        //        }
        //        return 0;
        //    }
        //}
    }
}
