﻿using System.ComponentModel.DataAnnotations;

namespace FA_Management.Data
{
    public class OutputStandard
    {
        [Key]
        public int Output_id { get; set; }
        public string Output_name { get; set; }
        public string Description { get; set; }
    }
}
