﻿using Microsoft.AspNetCore.Identity;

namespace FA_Management.Data
{
    public class ApplicationUser:IdentityUser
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime Birthday { get; set; }
        public bool Gender { get; set; }
        public bool Status { get; set; }
        public double Base_salary { get; set; }
    }
}
