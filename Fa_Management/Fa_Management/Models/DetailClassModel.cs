﻿using FA_Management.Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA_Management.Models
{
    public class DetailClassModel
    {
        public int Class_id { get; set; }
        public int Progam_id { get; set; }
        public int Syllabus_id { get; set; }
        public int Status { get; set; }
    }
}
