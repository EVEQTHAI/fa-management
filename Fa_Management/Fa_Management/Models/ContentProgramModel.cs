﻿namespace FA_Management.Models
{
	public class ContentProgramModel
	{
		public int Content_program_id { get; set; }
		public bool Status { get; set; }
		public int Syllabus_id { get; set; }
		public int Program_id { get; set; }
	}
}
