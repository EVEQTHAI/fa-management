﻿using FA_Management.Data;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;

namespace FA_Management.Models
{
	public class CalendarModel
	{
		
		public int Calendar_id { get; set; }
		public DateTime Assigned_datetime { get; set; }
		public int Class_id { get; set; }
	}
}
