﻿using FA_Management.Data;
using Microsoft.AspNetCore.Identity;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA_Management.Models
{
	public class ClassModel
	{
		public int Class_id { get; set; }
        public string Class_name { get; set; }
        public string Class_code { get; set; }
		public DateTime Create_on { get; set; }
		public int Duration { get; set; }
		public string Class_time { get; set; }
		public DateTime Start_date { get; set; }
		public DateTime End_date { get; set; }
		public string Attendee_type { get; set; }
		public int Attendee_amount { get; set; }
		public string Location { get; set; }
		public bool Status { get; set; }
	}
	public class ClassModelCreate
	{
		public string Class_code { get; set; }
        public string Class_name { get; set; }
        public DateTime Create_on { get; set; }
		public int Duration { get; set; }
		public string Class_time { get; set; }
		public DateTime Start_date { get; set; }
		public DateTime End_date { get; set; }
		public string Attendee_type { get; set; }
		public int Attendee_amount { get; set; }
		public string Location { get; set; }
		public bool Status { get; set; }
	}
	public class UpdateClassModel
	{
        public string Class_name { get; set; }
        public DateTime Create_on { get; set; }
        public int Duration { get; set; }
        public string Class_time { get; set; }
        public DateTime Start_date { get; set; }
        public DateTime End_date { get; set; }
        public string Attendee_type { get; set; }
        public int Attendee_amount { get; set; }
        public string Location { get; set; }
        public bool Status { get; set; }
    }
}
