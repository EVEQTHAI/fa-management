﻿using FA_Management.Data;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;
using System.Runtime.Serialization;

namespace FA_Management.Models
{
    public class SyllabusModel
    {
        public int Syllabus_id { get; set; }
        public string Syllabus_name { get; set; }
        public DateTime Create_on { get; set; }
        public string Create_by { get; set; }
        public int Duration { get; set; }
        public string Level { get; set; }
        public int Attendee_number { get; set; }
        public string Technical_requirement { get; set; }
        public string Objective_source { get; set; }
        public string Code { get; set; }

        [JsonIgnore]
        public int Version_i1 { get; set; }
        [JsonIgnore]
        public int Version_i2 { get; set; }
        [JsonIgnore]
        public int Version_i3 { get; set; }

        public string Version
        {
            get
            {
                return $"{Version_i1}.{Version_i2}.{Version_i3}";
            }
            set
            {
                var parts = value.Split('.');
                if (parts.Length >= 3 &&
                    int.TryParse(parts[0], out int i1) &&
                    int.TryParse(parts[1], out int i2) &&
                    int.TryParse(parts[2], out int i3))
                {
                    Version_i1 = i1;
                    Version_i2 = i2;
                    Version_i3 = i3;
                }
                else
                {
                }
            }
        }
        public int Status { get; set; }
        public int Outline_id { get; set; }
        public int AS_id { get; set; }
        public int Output_id { get; set; }
        public int Principle_id { get; set; }
    }
    public class SyllabusModelUpdate
    {
        public string Syllabus_name { get; set; }
        public int Duration { get; set; }
        public string Level { get; set; }
        public int Status { get; set; }
    }
    public class SyllabusCVS
    {
        public string Syllabus_name { get; set; }
        public DateTime Create_on { get; set; }
        public string Create_by { get; set; }
        public int Duration { get; set; }
        public string Level { get; set; }
        public int Attendee_number { get; set; }
        public string Technical_requirement { get; set; }
        public string Objective_source { get; set; }
        public string Code { get; set; }
        public double Version { get; set; }
        public int Status { get; set; }
        public int Outline_id { get; set; }
        public int AS_id { get; set; }
        public int Output_id { get; set; }
        public int Principle_id { get; set; }
    }

    public class MaterialModel
    {
        public int Material_id { get; set; }
        public string File_name { get; set; }
        public string Content_type { get; set; }
        public long? File_size { get; set; }
        public string Upload_by { get; set; }
        public DateTime Upload_date { get; set; }
        public bool Status { get; set; }
    }
    
}
