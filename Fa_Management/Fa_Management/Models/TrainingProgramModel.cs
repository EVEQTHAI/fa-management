﻿using CsvHelper.Configuration;
using FA_Management.Data;
using System.ComponentModel.DataAnnotations.Schema;

namespace FA_Management.Models
{
	public class TrainingProgramModel
	{
		public int Program_id { get; set; }
		public string Program_name { get; set; }
		public string Information { get; set; }
		public DateTime Create_on { get; set; }
		public string Create_by { get; set; }
		public int Status { get; set; }
		public int Duration { get; set; }
	}
	public class TrainingProgramModelCreate
	{
		public string Program_name { get; set; }
		public string Information { get; set; }
		public DateTime Create_on { get; set; }
		public string Create_by { get; set; }
		public int Status { get; set; }
		public int Duration { get; set; }
	}
	public class TrainingProgramModelUpdate {
		public string Program_name { get; set; }
		public string Information { get; set; }
		public DateTime Create_on { get; set; }
		public string Create_by { get; set; }
		public int Status { get; set; }
		public int Duration { get; set; }
	}
	
	public class TemplateTrainingProgram
	{
        public string Program_name { get; set; }
        public string Information { get; set; }
    }
}
