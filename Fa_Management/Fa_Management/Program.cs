using FA_Management.Data;
using FA_Management.Repository.IRepository;
using FA_Management.Repository;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using FA_Management.Helper;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

//5.Registration AutoMapper
builder.Services.AddAutoMapper(typeof(Program));
//Task Syllabus
builder.Services.AddScoped<ISyllabusRepository, SyllabusRepository>();
//Task Account
builder.Services.AddScoped<IAccountRepository, AccountRepository>();
//Task file CSV
builder.Services.AddScoped<ICSVRepository,CSVRepository>();
builder.Services.AddScoped<IMaterialRepository, MaterialRepository>();
//Task Class
builder.Services.AddScoped<IClassRepository, ClassRepository>();
//Task Training Program
builder.Services.AddScoped<ITrainingProgramRepository, TrainingProgramRepository>();
//Task DetailClass
builder.Services.AddScoped<IDetailClassRepository, DetailClassRepository>();
//Task Calendar
builder.Services.AddScoped<ICalendarRepository, CalendarRepository>();
//Task  Content Class
builder.Services.AddScoped<IContentClassRepository, ContentClassRepository>();
//Task Content Program
builder.Services.AddScoped<IContentProgramRepository, ContentProgramRepository>();


//1.
builder.Services.AddCors(opt => opt.AddDefaultPolicy(policy => policy.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader()));
//2.
builder.Services.AddIdentity<ApplicationUser, IdentityRole>()
    .AddEntityFrameworkStores<ApplicationDbContext>().AddDefaultTokenProviders();
//3.Authen JWT
builder.Services.AddAuthentication(options =>
{
    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
    options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
}).AddJwtBearer(option =>
{
    option.SaveToken = true;
    option.RequireHttpsMetadata = false;
    option.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
    {
        ValidateIssuer = true,
        ValidateAudience = true,
        ValidAudience = builder.Configuration["JWT:ValidAudience"],
        ValidIssuer = builder.Configuration["JWT:ValidIssuer"],
        IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(builder.Configuration["JWT:Secret"]))
    };
});
//4. ConnectionString
builder.Services.AddDbContext<ApplicationDbContext>(options =>
{
    options.UseSqlServer(builder.Configuration.GetConnectionString("FA_Manager_DB"));
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
