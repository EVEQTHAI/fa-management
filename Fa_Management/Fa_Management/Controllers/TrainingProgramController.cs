
﻿using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA_Management.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class TrainingProgramController : ControllerBase
	{
		private readonly ITrainingProgramRepository _repo;
		private readonly ICSVRepository _csvRepo;

		public TrainingProgramController(ITrainingProgramRepository repo, ICSVRepository csvRepo)
		{
			_repo = repo;
			_csvRepo = csvRepo;
		}
		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			return Ok(await _repo.GetAllProgram());
		}
		[HttpGet("{id}")]
		public async Task<IActionResult> GetById(int id)
		{
			var program = await _repo.GetById(id);
			return program == null ? NotFound() : Ok(program);
		}
		[HttpPost]
		public async Task<IActionResult> CreateNewProgram(TrainingProgramModelCreate model)
		{
			try {
				var newProgram = await _repo.CreateNewProgram(model);
				var program = await _repo.GetById(newProgram);
				return program == null ? NotFound() : Ok(program);
			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}
		}
		[HttpPut("{id}")]
		public async Task<IActionResult> UpdateProgram(int id, TrainingProgramModelUpdate model)
		{
			try
			{
				await _repo.UpdateProgramAsync(id, model);
				return Ok();
			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}

		}

		[HttpPost("Import")]
		public async Task<IActionResult> ImportByFileCsv(IFormFileCollection file)
		{
			var tpr = _csvRepo.ReadCSV<TrainingProgramModel>(file[0].OpenReadStream());
			foreach (var item in tpr)
			{
				_repo.Create(item);
			}
			return Ok(tpr);
		}
		[HttpPost("download-template")]
		public async Task<IActionResult> DownladTemlate(TemplateTrainingProgram model)
		{
			try
			{
				_csvRepo.GetTemplate(model);
				return Ok();
			} catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}
		}

		[HttpPost("{id}/Duplicate")]
		public async Task<IActionResult> DuplicateUpdate(int id, TrainingProgramModelUpdate model)
		{
            try
            {
				await _repo.DuplicateAsync(id, model);
                return Ok();
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
//    }
//}
//=======
//﻿using FA_Management.Models;
//using FA_Management.Repository.IRepository;
//using Microsoft.AspNetCore.Http;
//using Microsoft.AspNetCore.Mvc;

//namespace FA_Management.Controllers
//{
//	[Route("api/[controller]")]
//	[ApiController]
//	public class TrainingProgramController : ControllerBase
//	{
//		private readonly ITrainingProgramRepository _repo;


//		public TrainingProgramController(ITrainingProgramRepository repo)
//		{
//			_repo = repo;
//		}
		//[HttpGet]
		//public async Task<IActionResult> GetAll()
		//{
		//	return Ok(await _repo.GetAllProgram());
		//}
		//[HttpGet("{id}")]
		//public async Task<IActionResult> GetById(int id)
		//{
		//	var program = await _repo.GetById(id);
		//	return program == null ? NotFound() : Ok(program);
		//}
		//[HttpPost]
		//public async Task<IActionResult> CreateNewProgram(TrainingProgramModelCreate model)
		//{
		//	try {
		//		var newProgram = await _repo.CreateNewProgram(model);
		//		var program = await _repo.GetById(newProgram);
		//		return program == null ? NotFound() : Ok(program);
		//	}
		//	catch (Exception ex)
		//	{
		//		return BadRequest(ex.Message);
		//	}
		//}
		//[HttpPut("{id}")]
		//public async Task<IActionResult> UpdateProgram(int id,TrainingProgramModelUpdate model)
		//{		
		//	try
		//	{
		//		await _repo.UpdateProgramAsync(id, model);
		//		return Ok();
		//	}
		//	catch (Exception ex) 
		//	{
		//		return BadRequest(ex.Message);
		//	}

		//}
		[HttpGet("getWaitList")]
		public async Task<IActionResult> ShowWaitTraining()
		{

			var programs = await _repo.GetAllWaitProgram();
			return programs == null ? NotFound() : Ok(programs);
			
		}
		[HttpPut("{id},{status}/status")]
		public async Task<IActionResult> UpdateStatusTraining(int id, int status)
		{
			var program = await _repo.GetById(id);
			if (program == null)
			{
				return NotFound();
			}
			await _repo.UpdateStatusTraining(id, status);

			return Ok();
		}

        [HttpPost("{id}/activate")]
        public async Task<ActionResult> ActivateTrainingProgram(int id)
        {
            var program = await _repo.GetById(id);

            if (program == null)
            {
                return NotFound();
            }

            program.Status = 1; // Set IsActivated to true (active)

            await _repo.UpdateProgramAsync(id, program);

            return Ok();
        }

        [HttpPost("{id}/deactivate")]
        public async Task<ActionResult> DeactivateTrainingProgram(int id)
        {
            var program = await _repo.GetById(id);

            if (program == null)
            {
                return NotFound();
            }

            program.Status = 0; // Set IsActivated to false (inactive)

            await _repo.UpdateProgramAsync(id, program);

            return Ok();
        }

        [HttpGet("search")]
        public async Task<ActionResult<List<TrainingProgramModel>>> SearchTrainingPrograms(string keyword, int status = 0)
        {
            var programs = await _repo.SearchPrograms(keyword, status);

            if (programs.Count == 0)
            {
                return NotFound("There's no record matching with your keyword.");
            }

            return Ok(programs);
        }
    }
}
