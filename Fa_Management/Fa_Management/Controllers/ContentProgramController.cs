﻿using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA_Management.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ContentProgramController : ControllerBase
	{
		private readonly IContentProgramRepository _repo;

		public ContentProgramController(IContentProgramRepository repo)
		{
			_repo = repo;
		}

		[HttpPost("{program_id},{syllabus_id}/ContentProgram")]
		public async Task<IActionResult> CreateContentProgram(int program_id, int syllabus_id)
		{
			try
			{
				await _repo.CreateNewContent(program_id, syllabus_id);
				return Ok();

			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}
		}
	}
}
