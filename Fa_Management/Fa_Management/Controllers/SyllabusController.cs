﻿using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA_Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class SyllabusController : ControllerBase
    {
        private readonly ISyllabusRepository _repo;
        private readonly ICSVRepository _csvRepo;
        private readonly IMaterialRepository _materialRepo;
        private readonly IWebHostEnvironment _environment;

        public SyllabusController(ISyllabusRepository repo, ICSVRepository csvRepo, IMaterialRepository materialRepo, IWebHostEnvironment environment)
        {
            _repo = repo;
            _csvRepo = csvRepo;
            _materialRepo = materialRepo;
            _environment = environment;
        }

        [HttpPut("{id}")]
        public async Task<IActionResult> UpdateSyllabus(int id, SyllabusModelUpdate model)
        {
            try
            {
                bool check = _repo.Update(id, model);
                if (check)
                {
                    return Ok(model);
                }
                else
                {
                    return NotFound();
                }
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }

        }

        [HttpPost("read-file-csv")]
        public async Task<IActionResult> GetSyllabusCSV([FromForm] IFormFileCollection file)
        {
            var syllabusCSV = _csvRepo.ReadCSV<SyllabusCVS>(file[0].OpenReadStream());
            //return Ok(syllabusCSV); 
            foreach (var item in syllabusCSV)
            {
                _repo.Create(item);
            }
            return Ok(syllabusCSV);
        }
        [HttpPost("write-file-csv")]
        public async Task<IActionResult> WriteSyllabusCSV([FromBody] List<SyllabusCVS> mode)
        {
            _csvRepo.WriteCSV<SyllabusCVS>(mode);
            return Ok();
        }

        [HttpGet]
        public async Task<IActionResult> GetAllSyllabus(int page = 1, int pageSize = 10, string sortBy = "Create_on", string sortOrder = "desc")
        {
            try
            {
                var syllabusList = await _repo.GetAllAsync(page, pageSize, sortBy, sortOrder);

                if (syllabusList == null)
                {
                    return NotFound(); // Or return an appropriate response for the null case
                }

                //var result = syllabusList.Select(syllabus => new
                //{
                //    syllabus.Syllabus_name,
                //    syllabus.Code,
                //    syllabus.Create_on,
                //    syllabus.Create_by,
                //    syllabus.Output_id,
                //    Duration = syllabus.Duration
                //});

                return Ok(syllabusList);
            }
            catch (Exception ex)
            {
                // Handle exception and return appropriate response
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        [HttpPost]
        public async Task<IActionResult> CreateSyllabus(SyllabusModel syllabusModel, bool isDraft = false)
        {
            if (!ModelState.IsValid)
            {
                var errors = ModelState.Values.SelectMany(v => v.Errors)
                    .Select(e => e.ErrorMessage)
                    .ToList();
                return BadRequest(errors);
            }

            try
            {
                var syllabus = new Syllabus
                {
                    Syllabus_name = syllabusModel.Syllabus_name,
                    Create_on = DateTime.Now,
                    Create_by = syllabusModel.Create_by,
                    Duration = syllabusModel.Duration,
                    Level = syllabusModel.Level,
                    Attendee_number = syllabusModel.Attendee_number,
                    Technical_requirement = syllabusModel.Technical_requirement,
                    Objective_source = syllabusModel.Objective_source,
                    Code = GenerateCode(), // Add the code generation logic here
                    Version = syllabusModel.Version,
                    Status = syllabusModel.Status,
                    Outline_id = syllabusModel.Outline_id,
                    AS_id = syllabusModel.AS_id,
                    Output_id = syllabusModel.Output_id,
                    Principle_id = syllabusModel.Principle_id,
                };

                await _repo.AddAsync(syllabus);
                return Ok(syllabus);
            }
            catch (Exception ex)
            {
                // Handle exception and return appropriate response
                return StatusCode(500, $"An error occurred: {ex.Message}");
            }
        }

        private string GenerateCode()
        {

            return "";
        }

        [HttpGet("{key}")]
        public async Task<IActionResult> Search(string key)
        {
            var sy = await _repo.SearchByKeyAsync(key);
            if (sy != null || sy.Count() != 0)
            {
                return Ok(sy);
            }
            else
                return BadRequest();
        }
        [HttpPost("{id},{ver}/Duplicate")]
        public async Task<IActionResult> Duplicate(int id, int ver, SyllabusModelUpdate modelUpdate)
        {
            try
            {
				await _repo.DuplicateAsync(id, ver, modelUpdate);
				return Ok();
			} catch(Exception ex)
            {
                return BadRequest(ex);
            }
            
        }
    //}
//}


        [HttpPut("{id}/generateVersion")]
        public async Task<IActionResult> GenerateVersion(int id, SyllabusModel model)
        {
            try
            {
                if (await _repo.SyllabusExistsAsync(id))
                {
                    var updatedSyllabus = await _repo.GenerateVersionAsync(id, model);
                    if (updatedSyllabus != null)
                    {
                        return Ok(updatedSyllabus);
                    }
                    else
                    {
                        return NotFound("Update error!");
                    }
                }
                else
                {
                    return NotFound("Syllabus not found!");
                }
            }
            catch
            {
                return BadRequest();
            }

        }

        [HttpGet("GetAllMaterials")]
        public async Task<IActionResult> GetAllMaterials()
        {
            return Ok(await _materialRepo.GetAllMaterialsAync());
        }

        [HttpPost("UploadMaterialFile")]
        public async Task<IActionResult> UploadMaterial(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);
            var rootPath = Path.Combine(_environment.ContentRootPath, "Resources", "Learning Material");
            if (!Directory.Exists(rootPath))
            {
                Directory.CreateDirectory(rootPath);
            }
            MaterialModel uploadedMaterial = new MaterialModel();
            foreach (var file in files)
            {
                var upload = await _materialRepo.UploadFileMaterialAsync(file, rootPath, size);
                uploadedMaterial = upload;
            }
            return Ok(uploadedMaterial);
        }

        [HttpPost("DownloadMaterialFile/{id}")]
        public async Task<IActionResult> DownloadMaterial(int id)
        {
            var filePath = Path.Combine(_environment.ContentRootPath, "Resources", "Learning Material");
            var downloadMaterial = await _materialRepo.DownloadFileMaterialAsync(filePath, id);

            if (downloadMaterial != null)
            {
                return downloadMaterial;
            }
            else
            {
                return NotFound("Invalid file!"); 
            }
        }

        [HttpDelete("DeleteMaterial/{id}")]
        public async Task<IActionResult> DeleteMaterial(int id)
        {
            try
            {
                var filePath = Path.Combine(_environment.ContentRootPath, "Resources", "Learning Material");
                var isDeleted = await _materialRepo.DeleteMaterialAsync(id, filePath);

                if (isDeleted)
                {
                    return Ok("Material deleted successfully.");
                }
                else
                {
                    return NotFound("Material not found.");
                }
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
