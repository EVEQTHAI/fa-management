
﻿using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace FA_Management.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class ClassController : ControllerBase
	{
		private readonly IClassRepository _repo;


		public ClassController(IClassRepository repo)
		{
			_repo = repo;
		}
		[HttpGet]
		public async Task<IActionResult> GetAll()
		{
			return Ok(await _repo.GetAllClass());
		}
		[HttpGet("GetClassID/{id}")]
		public async Task<IActionResult> GetById(int id)
		{
			var program = await _repo.GetById(id);
			return program == null ? NotFound() : Ok(program);
		}
		[HttpPost("Create Class")]
		public async Task<IActionResult> CreateNewClass(ClassModelCreate model)
		{
			try
			{
				var newProgram = await _repo.CreateNewClass(model);
				var program = await _repo.GetById(newProgram);
				return program == null ? NotFound() : Ok(program);
			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}
		}
		[HttpPut("UpdateClass/{id}")]
		public async Task<IActionResult> UpdateClass(int id, UpdateClassModel model)
		{
			try
			{
				bool check = _repo.UpdateClass(id, model);
				if (check)
				{
					return Ok(model);
				}
				else
				{
					return NotFound();
				}
			}
			catch (Exception ex)
			{
				return BadRequest(ex.Message);
			}
		}
        // Delete Class
        [HttpDelete("DeleteClass/{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _repo.DeleteClass(id);
            
            if (result)
            {
                return Ok();      
            }
            else
            {
                return NotFound();
            }
        }
    }
}
