﻿using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace FA_Management.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DetailClassController : ControllerBase
    {
        private readonly IDetailClassRepository _repo;

        public DetailClassController(IDetailClassRepository repo)
        {
            _repo = repo;
        }

        [HttpPost("{program_id},{class_id}")]
        public async  Task<IActionResult> CreateDetailClass(int program_id, int class_id)
        {
            try
            {
                await _repo.CreateClassDetail(program_id, class_id);
                return Ok();
                
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
