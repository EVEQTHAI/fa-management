﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.EntityFrameworkCore;
using System;

namespace FA_Management.Repository
{
	public class TrainingProgramRepository : ITrainingProgramRepository
	{
		private readonly ApplicationDbContext _context;
		private readonly IMapper _mapper;

		public TrainingProgramRepository(ApplicationDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

        public void Create(TrainingProgramModel model)
        {
            var trainingProgram = _mapper.Map<TrainingProgram>(model);
            _context.TrainingProgram!.Add(trainingProgram);
            _context.SaveChanges();
        }

        public async Task<int> CreateNewProgram(TrainingProgramModelCreate model)
		{
			var newProgram = _mapper.Map<TrainingProgram>(model);
			_context.TrainingProgram.Add(newProgram);
			await _context.SaveChangesAsync();
			return newProgram.Program_id;
		}

		public async Task<List<TrainingProgramModel>> GetAllProgram()
		{
			var program = await _context.TrainingProgram.ToListAsync();
			return _mapper.Map<List<TrainingProgramModel>>(program);
		}

		public async Task<TrainingProgramModel> GetById(int id)
		{
			var program = await _context.TrainingProgram.FindAsync(id);
			return _mapper.Map<TrainingProgramModel>(program);
		}

		public async Task UpdateProgramAsync(int id, TrainingProgramModelUpdate model)
		{
			var check = await _context.TrainingProgram.FindAsync(id);
			if (check != null)
			{
				check.Program_name = model.Program_name;
				check.Information = model.Information;
				check.Status = model.Status;
				check.Duration = model.Duration;
				check.Create_on = model.Create_on;
				await _context.SaveChangesAsync();
			}
		}
		public async Task<List<TrainingProgramModel>> GetAllWaitProgram()
		{
			var program = await _context.TrainingProgram.ToListAsync();
			return _mapper.Map<List<TrainingProgramModel>>(program.Where(t => t.Status == 0));
		}
		public async Task UpdateStatusTraining(int id,int status)
		{
            var program = await _context.TrainingProgram.FindAsync(id);
            program.Status = status;
			await _context.SaveChangesAsync();
		}

		public async Task UpdateProgramAsync(int id, TrainingProgramModel model)
		{
			var program = await _context.TrainingProgram.FindAsync(id);

			if (program != null)
			{
				program.Program_name = model.Program_name;
				program.Information = model.Information;
				program.Status = model.Status;
				program.Duration = model.Duration;

				await _context.SaveChangesAsync();
			}
		}
		public async Task DuplicateAsync(int id, TrainingProgramModelUpdate model )
		{
            var trainingProgram = _mapper.Map<TrainingProgram>(model);
            var check = await _context.TrainingProgram.FindAsync (id);
			if(check != null )
			{
				trainingProgram.Program_name = check.Program_name;
				trainingProgram.Information = check.Information;
				trainingProgram.Status = 0;
				trainingProgram.Duration = check.Duration;
				trainingProgram.Create_on = DateTime.Now;
			}
			await _context.TrainingProgram!.AddAsync(trainingProgram);
			_context.SaveChanges();
        }
  //  }
		//}

		public async Task<List<TrainingProgramModel>> SearchPrograms(string keyword, int status)
		{
			var programs = await _context.TrainingProgram
				.Where(p => p.Program_name.Contains(keyword) && (p.Status == status))
				.ToListAsync();

			return _mapper.Map<List<TrainingProgramModel>>(programs);
		}
	}
}

        
