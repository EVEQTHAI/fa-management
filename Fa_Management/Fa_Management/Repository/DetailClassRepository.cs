﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace FA_Management.Repository
{
    public class DetailClassRepository : IDetailClassRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public DetailClassRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task CreateClassDetail(int program_id,int class_id)
        {
            var syllabus = FindAllSylabusOfProgram(program_id);
            foreach (var item in syllabus)
            {
                var detail = new DetailClassModel
                {
                    Status = 1,
                    Progam_id = program_id,
                    Class_id = class_id,
                    Syllabus_id = item.Syllabus_id
                };
                var mapper = _mapper.Map<DetailClass>(detail);
                await _context.DetailClass!.AddAsync(mapper);
            }
            await _context.SaveChangesAsync();
        }

        public List<ContentProgram> FindAllSylabusOfProgram(int id)
        {
            var syllabus = _context.ContentProgram!.Where(p=>p.Program_id == id).ToList();
            return syllabus;
        }
    }
}
