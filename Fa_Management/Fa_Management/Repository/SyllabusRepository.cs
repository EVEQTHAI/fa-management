﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;

namespace FA_Management.Repository
{
    public class SyllabusRepository: ISyllabusRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;

        public SyllabusRepository(ApplicationDbContext context,IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        
        public void Create(SyllabusCVS model)
        {
            var syllabus = _mapper.Map<Syllabus>(model);
             _context.Syllabus!.Add(syllabus);
             _context.SaveChanges();
        }

        public bool Update(int id, SyllabusModelUpdate model)
        {
            var syllabus =  _context.Syllabus.Find(id);
            if (syllabus != null)
            {
                syllabus.Syllabus_name = model.Syllabus_name;
                syllabus.Duration = model.Duration;
                syllabus.Level = model.Level;
                syllabus.Status = model.Status;
                 _context.SaveChangesAsync();
                return true;
            }
            else
            {
                return false;
            }
        }

        public async Task<List<SyllabusModel>> GetAllAsync(int page, int pageSize, string sortBy, string sortOrder)
        {
            var query = _context.Syllabus.AsQueryable();

            // Apply sorting
            switch (sortBy)
            {
                case "Create_on":
                    query = (sortOrder == "asc") ? query.OrderBy(tp => tp.Create_on) : query.OrderByDescending(tp => tp.Create_on);
                    break;
                // Add additional cases for other sortable properties if needed
                default:
                    // Use a default sorting option if no valid sortBy value is provided
                    query = query.OrderBy(tp => tp.Syllabus_id);
                    break;
            }

            // Apply pagination
            var totalCount = await query.CountAsync();
            var totalPages = (int)Math.Ceiling(totalCount / (double)pageSize);
            var currentPage = Math.Clamp(page, 1, totalPages);
            var skipCount = (currentPage - 1) * pageSize;

            query = query.Skip(skipCount).Take(pageSize);

            var syllabusList = await query.ToListAsync();
            var syllabusModels = _mapper.Map<List<SyllabusModel>>(syllabusList);

            return syllabusModels;
        }

        public async Task<List<SyllabusModel>> SearchByKeyAsync(string key)
        {
            var syllabus = await _context.Syllabus.ToListAsync();
            return _mapper.Map < List < SyllabusModel >> (syllabus.Where(s => s.Syllabus_name.Contains(key) || s.Code.Contains(key)).ToList());

		}
        

        public async Task<SyllabusModel> GenerateVersionAsync(int id, SyllabusModel model)
        {
            var syllabus = await _context.Syllabus.FirstOrDefaultAsync(s => s.Syllabus_id == id);

            if (syllabus != null)
            {
                var v1 = model.Version_i1;
                
                syllabus.Version_i2++;
                //syllabus.Version_i3++;

                syllabus.Version = $"{v1}.{syllabus.Version_i2}.{syllabus.Version_i3}";
                _context.Update(syllabus);

                try
                {
                    await _context.SaveChangesAsync();
                    return _mapper.Map<SyllabusModel>(syllabus);
                }
                catch
                {
                    return null;
                }
            }
            return null;
        }

        public async Task<bool> SyllabusExistsAsync(int id)
        {
            return await _context.Syllabus.AnyAsync(s => s.Syllabus_id == id);
        }

		public async Task DuplicateAsync(int id, int ver, SyllabusModelUpdate model)
		{
			var syl = await _context.Syllabus.ToListAsync();
			var sy = syl.Where(s => s.Syllabus_id == id).FirstOrDefault();

            var duplicateSyllabus = _mapper.Map<Syllabus>(sy);
            duplicateSyllabus.Syllabus_id = 0;
			if (model != null)
			{
				duplicateSyllabus.Syllabus_name = model.Syllabus_name;
				duplicateSyllabus.Duration = model.Duration;
				duplicateSyllabus.Level = model.Level;
				duplicateSyllabus.Status = model.Status;

				
			}
			if (sy.Version_i1 != ver)
			{
				duplicateSyllabus.Version_i2 = 0;
			}
			else
			{
				duplicateSyllabus.Version_i2++;
			}
			duplicateSyllabus.Version_i1 = ver;
            
			duplicateSyllabus.Version = $"{duplicateSyllabus.Version_i1}.{duplicateSyllabus.Version_i2}.{duplicateSyllabus.Version_i3}";
			await AddAsync(duplicateSyllabus);
            
        }

        public async Task AddAsync(Syllabus syllabus)
        {
            await _context.Set<Syllabus>().AddAsync(syllabus);
            await _context.SaveChangesAsync();
        }


    }

}
