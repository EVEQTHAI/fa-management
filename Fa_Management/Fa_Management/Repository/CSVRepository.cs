﻿using CsvHelper;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using System.Formats.Asn1;
using System.Globalization;

namespace FA_Management.Repository
{
    public class CSVRepository : ICSVRepository
    {
        public void GetTemplate<T>(T record)
        {
            var csvPath = Path.Combine(Environment.CurrentDirectory, $"Trainig Program-{DateTime.Now.ToFileTime}.csv");
            using (var streamWriter = new StreamWriter(csvPath))
            {
                using (var csvWriter = new CsvWriter(streamWriter, CultureInfo.InvariantCulture))
                {
                    var property = typeof(T).GetProperties().Select(p => p.Name);
                    foreach (var propertyValue in property)
                    {
                        csvWriter.WriteField(propertyValue);
                    }
                }
            }
        }

        public IEnumerable<T> ReadCSV<T>(Stream file)
        {
            var reader = new StreamReader(file);
            var csv = new CsvReader(reader, CultureInfo.InvariantCulture);

            var records = csv.GetRecords<T>();
            return records;
        }

        public void WriteCSV<T>(List<T> records)
        {
            using (var writer = new StreamWriter("D:\\file.csv"))
            using (var csv = new CsvWriter(writer, CultureInfo.InvariantCulture))
            {
                csv.WriteRecords(records);
            }
        }
    }
}
