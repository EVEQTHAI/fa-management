﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;

namespace FA_Management.Repository
{
	public class ContentProgramRepository : IContentProgramRepository
	{
		private readonly ApplicationDbContext _context;
		private readonly IMapper _mapper;

		public ContentProgramRepository(ApplicationDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}
		public async Task CreateNewContent(int id_program, int id_syllabus)
		{
			var syllabus = FindSylabusOfProgram(id_program);
			foreach (var item in syllabus)
			{
				var detail = new ContentProgramModel
				{
					Status = true,
					Program_id	 = id_program,
					Syllabus_id = id_syllabus,
				};
				var mapper = _mapper.Map<ContentProgram>(detail);
				await _context.ContentProgram!.AddAsync(mapper);
			}
			await _context.SaveChangesAsync();
		}
		public List<Syllabus> FindSylabusOfProgram(int id)
		{
			var syllabus = _context.Syllabus!.Where(p => p.Syllabus_id == id).ToList();
			return syllabus;
		}
	}
}
