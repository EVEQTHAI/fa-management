
﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Internal;

namespace FA_Management.Repository
{
	public class ClassRepository : IClassRepository
	{
		private readonly ApplicationDbContext _context;
		private readonly IMapper _mapper;
		

		public ClassRepository(ApplicationDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}
		public async Task<int> CreateNewClass(ClassModelCreate model)
		{
			var newClass = _mapper.Map<Class>(model);
			_context.Class.Add(newClass);
			await _context.SaveChangesAsync();
			return newClass.Class_id;
		}


        public async Task<List<ClassModel>> GetAllClass()
		{
			var Classs = await _context.Class.ToListAsync();
			return _mapper.Map<List<ClassModel>>(Classs);
		}

		public async Task<ClassModel> GetById(int id)
		{
			var Classs = await _context.Class.FindAsync(id);
			return _mapper.Map<ClassModel>(Classs);
		}

		public bool UpdateClass(int id, UpdateClassModel model)
		{
			var classes = _context.Class.Find(id);
			if (classes != null)
			{
				classes.Class_name = model.Class_name;
				classes.Duration = model.Duration;
				classes.Start_date = model.Start_date;
				classes.End_date = model.End_date;
				classes.Attendee_type = model.Attendee_type;
				classes.Attendee_amount = model.Attendee_amount;
				classes.Location = model.Location;
				classes.Status = model.Status;
				_context.SaveChangesAsync();
				return true;
			}
			else
			{
				return false;
			}
		}

        public async Task<List<ClassModel>> SearchClass(string key)
        {
            var Classs = await _context.Class.ToListAsync();
            var result = Classs.Where(c => c.Class_code.Contains(key) || c.Class_name.Contains(key)).ToList();
            return _mapper.Map<List<ClassModel>>(Classs);
        }

        //delete class by id
        public async Task<bool> DeleteClass(int id)
        {
            var classToDelete = await _context.Class.FindAsync(id);
            //find class by id

            if (classToDelete != null)  //if the class exists
            {
                _context.Class.Remove(classToDelete);     //remove class from context
                await _context.SaveChangesAsync();		  //save changes to DB
                return true;							  //indicate successful deletion
            }
            else
            {
                return false;							  // Return false if class does not exist
            }
        }
    }
}
