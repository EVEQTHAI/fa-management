﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.EntityFrameworkCore;

namespace FA_Management.Repository
{
	public class CalendarRepository: ICalendarRepository
	{
		private readonly ApplicationDbContext _context;
		private readonly IMapper _mapper;


		public CalendarRepository(ApplicationDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}

		public async Task<List<CalendarModel>> GetAllCalendar()
		{
			var calendars = await _context.Calendar.ToListAsync();
			return _mapper.Map<List<CalendarModel>>(calendars);
		}

	}
}
