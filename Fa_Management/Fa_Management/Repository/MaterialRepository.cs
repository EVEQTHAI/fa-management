﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Models;
using FA_Management.Repository.IRepository;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;
using Microsoft.EntityFrameworkCore;

namespace FA_Management.Repository
{
    public class MaterialRepository : IMaterialRepository
    {
        private readonly ApplicationDbContext _context;
        private readonly IMapper _mapper;
        public MaterialRepository(ApplicationDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }
        public async Task<List<MaterialModel>> GetAllMaterialsAync()
        {
            var material = await _context.LearningMaterial.Where(m => m.Status == true).ToListAsync();
            return _mapper.Map<List<MaterialModel>>(material);
        }
        public async Task<IActionResult?> DownloadFileMaterialAsync(string filePath, int id)
        {
            string contentType;
            byte[] fileBytes;

            var provider = new FileExtensionContentTypeProvider();
            var material = await _context.LearningMaterial.FindAsync(id);
            if (material == null)
            {
                return null;
            }
            var downloadMaterial = _mapper.Map<MaterialModel>(material);
            var downloadPath = Path.Combine(filePath, downloadMaterial.File_name);
            
            if (!provider.TryGetContentType(filePath, out contentType))
            {
                contentType = "application/octet-stream";
            }

            if (material.Status == false)
            {
                return null;
            }
            else
            {
                fileBytes = File.ReadAllBytes(downloadPath);

                return new FileContentResult(fileBytes, contentType)
                {
                    FileDownloadName = downloadMaterial.File_name
                };
            }
        }

        public async Task<MaterialModel> UploadFileMaterialAsync(IFormFile file, string rootPath, long? size)
        {
            var filePath = Path.Combine(rootPath, file.FileName);
            using (var stream = new FileStream(filePath, FileMode.Create))
            {
                var material = new MaterialModel
                {
                    File_name = file.FileName,
                    Content_type = file.ContentType,
                    File_size = size,
                    Upload_by = "Admin",
                    Upload_date = DateTime.Now,
                    Status = true
                };

                await file.CopyToAsync(stream);
                var newMaterial = _mapper.Map<LearningMaterial>(material);
                _context.LearningMaterial.Add(newMaterial);
                await _context.SaveChangesAsync();

                return material;
            }
        }

        public async Task<bool> DeleteMaterialAsync(int id, string rootPath)
        {
            var material = await _context.LearningMaterial.FindAsync(id);
            if (material != null)
            {
                var filePath = Path.Combine(rootPath, material.File_name);
                File.Delete(filePath);

                material.Status = false;
                await _context.SaveChangesAsync();
                return true; 
            }
            return false; 
        }
    }
}
