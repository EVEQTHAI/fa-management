﻿using AutoMapper;
using FA_Management.Data;
using FA_Management.Repository.IRepository;

namespace FA_Management.Repository
{
	public class ContentClassRepository: IContentClassRepository
	{
		private readonly ApplicationDbContext _context;
		private readonly IMapper _mapper;


		public ContentClassRepository(ApplicationDbContext context, IMapper mapper)
		{
			_context = context;
			_mapper = mapper;
		}
	}
}
