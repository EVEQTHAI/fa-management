﻿namespace FA_Management.Repository.IRepository
{
    public interface ICSVRepository
    {
        public IEnumerable<T> ReadCSV<T>(Stream file);
        public void WriteCSV<T>(List<T> records);
        public void GetTemplate<T>(T record);
    }
}
