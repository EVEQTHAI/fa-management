﻿using FA_Management.Models;

namespace FA_Management.Repository.IRepository
{
	public interface IContentProgramRepository	
	{
		public Task CreateNewContent(int id_program, int id_syllabus);
	}
}
