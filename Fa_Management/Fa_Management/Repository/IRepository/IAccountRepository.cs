﻿using FA_Management.Models;
using Microsoft.AspNetCore.Identity;

namespace FA_Management.Repository.IRepository
{
    public interface IAccountRepository
    {
        public Task<IdentityResult> SignUpAsync(SignUpModel model);

        public Task<string> SignInAsync(SignInModel model);
		
	}
}
