﻿using FA_Management.Data;
using FA_Management.Models;

namespace FA_Management.Repository.IRepository
{
    public interface ISyllabusRepository
    {
        public bool Update(int id, SyllabusModelUpdate model);
        public void Create(SyllabusCVS model);
        public Task<List<SyllabusModel>> GetAllAsync(int page, int pageSize, string sortBy, string sortOrder);
        public Task<List<SyllabusModel>> SearchByKeyAsync(string key);
		public Task DuplicateAsync(int id, int ver, SyllabusModelUpdate model);
        public Task<SyllabusModel> GenerateVersionAsync(int id, SyllabusModel model);
        public Task<bool> SyllabusExistsAsync(int id);
        public Task AddAsync(Syllabus syllabus);

    }


}
