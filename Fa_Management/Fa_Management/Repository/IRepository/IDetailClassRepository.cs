﻿using FA_Management.Models;

namespace FA_Management.Repository.IRepository
{
    public interface IDetailClassRepository
    {
        public  Task CreateClassDetail(int program_id, int class_id);
    }
}
