﻿using FA_Management.Models;
using Microsoft.AspNetCore.Mvc;

namespace FA_Management.Repository.IRepository
{
    public interface IMaterialRepository
    {
        public Task<List<MaterialModel>> GetAllMaterialsAync();
        public Task<bool> DeleteMaterialAsync(int id, string rootPath);
        public Task<MaterialModel> UploadFileMaterialAsync(IFormFile file, string rootPath, long? size);
        public Task<IActionResult> DownloadFileMaterialAsync(string filePath, int id);
    }
}
