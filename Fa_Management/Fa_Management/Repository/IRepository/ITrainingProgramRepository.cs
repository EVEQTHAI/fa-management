﻿using FA_Management.Models;

namespace FA_Management.Repository.IRepository
{
	public interface ITrainingProgramRepository
	{
		public Task<List<TrainingProgramModel>> GetAllProgram();
		public Task<int>CreateNewProgram(TrainingProgramModelCreate model);
		public Task<TrainingProgramModel> GetById(int id);
		public Task UpdateProgramAsync(int id, TrainingProgramModelUpdate model);
        public void Create(TrainingProgramModel model);
		public Task DuplicateAsync(int id, TrainingProgramModelUpdate model);
		public Task<List<TrainingProgramModel>> GetAllWaitProgram();
		public Task UpdateStatusTraining(int id, int status);
        public Task UpdateProgramAsync(int id, TrainingProgramModel model);
        public Task<List<TrainingProgramModel>> SearchPrograms(string keyword, int status);
    }
}
