﻿using FA_Management.Models;

namespace FA_Management.Repository.IRepository
{
	public interface IClassRepository
	{
		public Task<List<ClassModel>> GetAllClass();
		public Task<int> CreateNewClass(ClassModelCreate model);
		public Task<ClassModel> GetById(int id);

		public Task<List<ClassModel>> SearchClass(string key);
        public bool UpdateClass(int id, UpdateClassModel model);
        public Task<bool> DeleteClass(int id);
    }
}
