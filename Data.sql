USE [FA_Manager_DB]
GO
SET IDENTITY_INSERT [dbo].[LearningMaterial] ON 

INSERT [dbo].[LearningMaterial] ([Material_id], [File_name], [Upload_by], [Upload_date], [Status], [Content_type], [File_size]) VALUES (1, N'FAMS_PracticalBA2_User-Story.xlsx', N'Admin', CAST(N'2023-10-09T08:39:20.9556980' AS DateTime2), 1, N'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', NULL)
SET IDENTITY_INSERT [dbo].[LearningMaterial] OFF
GO


USE [FA_Manager_DB]
GO
SET IDENTITY_INSERT [dbo].[AssessmentScheme] ON 

INSERT [dbo].[AssessmentScheme] ([AS_id], [Quiz], [Assignment], [Final], [Final_theory], [Final_Pactice], [Passing_gpa]) VALUES (1, 10, 10, 20, 30, 30, 50)
SET IDENTITY_INSERT [dbo].[AssessmentScheme] OFF
GO
SET IDENTITY_INSERT [dbo].[DeliveryPrinciple] ON 

INSERT [dbo].[DeliveryPrinciple] ([Principle_id], [Training], [Retest], [Marking], [Waiver_criteria], [Other]) VALUES (1, N'Training Good', N'Reses', N'adsasdadasd', N'Crasdasd ass', N'Ballser')
SET IDENTITY_INSERT [dbo].[DeliveryPrinciple] OFF
GO
SET IDENTITY_INSERT [dbo].[OutputStandard] ON 

INSERT [dbo].[OutputStandard] ([Output_id], [Output_name], [Description]) VALUES (1, N'H4SD', N'Coding Standar')
INSERT [dbo].[OutputStandard] ([Output_id], [Output_name], [Description]) VALUES (2, N'K6SD', N'Technical Standar')
INSERT [dbo].[OutputStandard] ([Output_id], [Output_name], [Description]) VALUES (3, N'H6SD', N'Management Standar')
SET IDENTITY_INSERT [dbo].[OutputStandard] OFF
GO
SET IDENTITY_INSERT [dbo].[DeliveryType] ON 

INSERT [dbo].[DeliveryType] ([Delivery_id], [Delivery_name]) VALUES (1, N'Assignment/Lab')
INSERT [dbo].[DeliveryType] ([Delivery_id], [Delivery_name]) VALUES (2, N'Concept/Lecture')
INSERT [dbo].[DeliveryType] ([Delivery_id], [Delivery_name]) VALUES (3, N'Guide/Review')
INSERT [dbo].[DeliveryType] ([Delivery_id], [Delivery_name]) VALUES (4, N'Test/Quiz')
INSERT [dbo].[DeliveryType] ([Delivery_id], [Delivery_name]) VALUES (5, N'Exam')
SET IDENTITY_INSERT [dbo].[DeliveryType] OFF
GO
SET IDENTITY_INSERT [dbo].[Unit] ON 

INSERT [dbo].[Unit] ([Unit_id], [Unit_name],[Output_standard], [Training_time], [Method], [Delivery_id], [Material_id]) VALUES (1, N'.Net Introduction', N'H4SD', 30, N'Online', 1, 1)
INSERT [dbo].[Unit] ([Unit_id], [Unit_name],[Output_standard], [Training_time], [Method], [Delivery_id], [Material_id]) VALUES (2, N'Operations', N'H4SD', 120, N'Offline', 5, 1)
SET IDENTITY_INSERT [dbo].[Unit] OFF
GO
SET IDENTITY_INSERT [dbo].[Outline] ON 

INSERT [dbo].[Outline] ([Outline_id], [Day_no], [Unit_id]) VALUES (1, 1, 1)
INSERT [dbo].[Outline] ([Outline_id], [Day_no], [Unit_id]) VALUES (2, 1, 2)
SET IDENTITY_INSERT [dbo].[Outline] OFF
GO
SET IDENTITY_INSERT [dbo].[Syllabus] ON 

INSERT [dbo].[Syllabus] ([Syllabus_id], [Syllabus_name], [Create_on], [Create_by], [Duration], [Level], [Attendee_number], [Technical_requirement], [Objective_source], [Code], [Version], [Status], [Outline_id], [AS_id], [Output_id], [Principle_id]) VALUES (1, N'C#', CAST(N'2023-11-11T00:00:00.0000000' AS DateTime2), N'AD', 15, N'Easy', 15, N'Tech', N'Objective', N'NPL', N'1.0.0', 1, 1, 1, 1, 1)
SET IDENTITY_INSERT [dbo].[Syllabus] OFF
GO

