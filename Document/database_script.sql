CREATE TABLE "Permission"(
    "detail_id" BIGINT NOT NULL,
    "create" BIT NOT NULL,
    "view" BIT NOT NULL,
    "modify" BIT NOT NULL,
    "denied" BIT NOT NULL,
    "full_access" BIT NOT NULL
);
ALTER TABLE
    "Permission" ADD CONSTRAINT "permission_detail_id_primary" PRIMARY KEY("detail_id");
CREATE TABLE "Delivery Principle"(
    "principle_id" BIGINT NOT NULL,
    "training" NVARCHAR(255) NULL,
    "retest" NVARCHAR(255) NOT NULL,
    "marking" NVARCHAR(255) NOT NULL,
    "waiver_criteria" NVARCHAR(255) NOT NULL,
    "others" NVARCHAR(255) NOT NULL
);
ALTER TABLE
    "Delivery Principle" ADD CONSTRAINT "delivery principle_principle_id_primary" PRIMARY KEY("principle_id");
CREATE TABLE "Outline"(
    "outline_id" BIGINT NOT NULL,
    "day_no" INT NOT NULL,
    "unit_id" BIGINT NOT NULL
);
ALTER TABLE
    "Outline" ADD CONSTRAINT "outline_outline_id_primary" PRIMARY KEY("outline_id");
CREATE INDEX "outline_unit_id_index" ON
    "Outline"("unit_id");
CREATE TABLE "Training Program"(
    "program_id" BIGINT NOT NULL,
    "program_name" BIGINT NOT NULL,
    "created_on" DATE NOT NULL,
    "created_by" NVARCHAR(255) NOT NULL,
    "duration" SMALLINT NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "syllabus_id" BIGINT NOT NULL
);
ALTER TABLE
    "Training Program" ADD CONSTRAINT "training program_program_id_primary" PRIMARY KEY("program_id");
CREATE INDEX "training program_syllabus_id_index" ON
    "Training Program"("syllabus_id");
CREATE TABLE "Unit"(
    "unit_id" BIGINT NOT NULL,
    "output_standard" VARCHAR(255) NOT NULL,
    "training_time" BIGINT NOT NULL,
    "method" VARCHAR(255) NOT NULL,
    "delivery_id" BIGINT NOT NULL
);
ALTER TABLE
    "Unit" ADD CONSTRAINT "unit_unit_id_primary" PRIMARY KEY("unit_id");
CREATE INDEX "unit_delivery_id_index" ON
    "Unit"("delivery_id");
CREATE TABLE "Syllabus"(
    "syllabus_id" BIGINT NOT NULL,
    "syllabus_name" BIGINT NOT NULL,
    "created_on" DATE NOT NULL,
    "created_by" BIGINT NOT NULL,
    "duration" BIGINT NOT NULL,
    "level" VARCHAR(255) NOT NULL,
    "attendee_number" BIGINT NOT NULL,
    "technical_requirement" NVARCHAR(255) NOT NULL,
    "objective_source" NVARCHAR(255) NOT NULL,
    "status" VARCHAR(255) NOT NULL,
    "outline_id" BIGINT NOT NULL,
    "assessment_id" BIGINT NOT NULL,
    "output_standard_id" BIGINT NOT NULL,
    "principle_id" BIGINT NOT NULL
);
ALTER TABLE
    "Syllabus" ADD CONSTRAINT "syllabus_syllabus_id_primary" PRIMARY KEY("syllabus_id");
CREATE INDEX "syllabus_outline_id_index" ON
    "Syllabus"("outline_id");
CREATE INDEX "syllabus_assessment_id_index" ON
    "Syllabus"("assessment_id");
CREATE INDEX "syllabus_output_standard_id_index" ON
    "Syllabus"("output_standard_id");
CREATE INDEX "syllabus_principle_id_index" ON
    "Syllabus"("principle_id");
CREATE TABLE "Calendar"(
    "calendar_id" BIGINT NOT NULL,
    "class_id" BIGINT NOT NULL,
    "assigned_datetime" DATETIME NOT NULL
);
ALTER TABLE
    "Calendar" ADD CONSTRAINT "calendar_calendar_id_primary" PRIMARY KEY("calendar_id");
CREATE INDEX "calendar_class_id_index" ON
    "Calendar"("class_id");
CREATE TABLE "Output Standard"(
    "output_name" VARCHAR(255) NOT NULL,
    "output_id" BIGINT NOT NULL,
    "description" VARCHAR(255) NULL
);
ALTER TABLE
    "Output Standard" ADD CONSTRAINT "output standard_output_id_primary" PRIMARY KEY("output_id");
CREATE TABLE "Role"(
    "role_id" BIGINT NOT NULL,
    "role_name" BIGINT NOT NULL,
    "permission_id" BIGINT NOT NULL
);
ALTER TABLE
    "Role" ADD CONSTRAINT "role_role_id_primary" PRIMARY KEY("role_id");
CREATE INDEX "role_permission_id_index" ON
    "Role"("permission_id");
CREATE TABLE "Class"(
    "class_id" BIGINT NOT NULL,
    "class_code" VARCHAR(255) NOT NULL,
    "created_on" DATE NOT NULL,
    "created_by" BIGINT NOT NULL,
    "duration" BIGINT NOT NULL,
    "class_time" VARCHAR(255) NOT NULL,
    "start_date" DATE NOT NULL,
    "end_date" DATE NOT NULL,
    "attendee_type" VARCHAR(255) NOT NULL,
    "attendee_amount" BIGINT NOT NULL,
    "location" VARCHAR(255) NOT NULL,
    "status" BIGINT NOT NULL,
    "user_id" BIGINT NOT NULL,
    "program_id" BIGINT NOT NULL
);
ALTER TABLE
    "Class" ADD CONSTRAINT "class_class_id_primary" PRIMARY KEY("class_id");
CREATE UNIQUE INDEX "class_class_code_unique" ON
    "Class"("class_code");
CREATE INDEX "class_user_id_index" ON
    "Class"("user_id");
CREATE INDEX "class_program_id_index" ON
    "Class"("program_id");
CREATE TABLE "User"(
    "user_id" BIGINT NOT NULL,
    "user_name" NVARCHAR(255) NOT NULL,
    "password" NVARCHAR(255) NOT NULL,
    "full_name" NVARCHAR(255) NOT NULL,
    "email" NVARCHAR(255) NOT NULL,
    "birthday" DATE NOT NULL,
    "gender" VARCHAR(255) NOT NULL,
    "type" VARCHAR(255) NOT NULL,
    "status" BIT NOT NULL,
    "role_id" BIGINT NOT NULL,
    "base_salary" BIGINT NULL
);
ALTER TABLE
    "User" ADD CONSTRAINT "user_user_id_primary" PRIMARY KEY("user_id");
CREATE INDEX "user_role_id_index" ON
    "User"("role_id");
CREATE TABLE "Assessment Scheme"(
    "id" BIGINT NOT NULL,
    "quiz" BIGINT NOT NULL,
    "assignment" BIGINT NOT NULL,
    "final" BIGINT NOT NULL,
    "final_theory" BIGINT NOT NULL,
    "final_practice" BIGINT NOT NULL,
    "passing_gpa" BIGINT NOT NULL
);
ALTER TABLE
    "Assessment Scheme" ADD CONSTRAINT "assessment scheme_id_primary" PRIMARY KEY("id");
CREATE TABLE "Delivery Type"(
    "delivery_id" BIGINT NOT NULL,
    "delivery_name" BIGINT NOT NULL
);
ALTER TABLE
    "Delivery Type" ADD CONSTRAINT "delivery type_delivery_id_primary" PRIMARY KEY("delivery_id");
ALTER TABLE
    "Syllabus" ADD CONSTRAINT "syllabus_outline_id_foreign" FOREIGN KEY("outline_id") REFERENCES "Outline"("outline_id");
ALTER TABLE
    "Training Program" ADD CONSTRAINT "training program_syllabus_id_foreign" FOREIGN KEY("syllabus_id") REFERENCES "Syllabus"("syllabus_id");
ALTER TABLE
    "Syllabus" ADD CONSTRAINT "syllabus_principle_id_foreign" FOREIGN KEY("principle_id") REFERENCES "Delivery Principle"("principle_id");
ALTER TABLE
    "Outline" ADD CONSTRAINT "outline_unit_id_foreign" FOREIGN KEY("unit_id") REFERENCES "Unit"("unit_id");
ALTER TABLE
    "Calendar" ADD CONSTRAINT "calendar_class_id_foreign" FOREIGN KEY("class_id") REFERENCES "Class"("class_id");
ALTER TABLE
    "Syllabus" ADD CONSTRAINT "syllabus_output_standard_id_foreign" FOREIGN KEY("output_standard_id") REFERENCES "Output Standard"("output_id");
ALTER TABLE
    "Role" ADD CONSTRAINT "role_permission_id_foreign" FOREIGN KEY("permission_id") REFERENCES "Permission"("detail_id");
ALTER TABLE
    "User" ADD CONSTRAINT "user_role_id_foreign" FOREIGN KEY("role_id") REFERENCES "Role"("role_id");
ALTER TABLE
    "Class" ADD CONSTRAINT "class_program_id_foreign" FOREIGN KEY("program_id") REFERENCES "Training Program"("program_id");
ALTER TABLE
    "Syllabus" ADD CONSTRAINT "syllabus_assessment_id_foreign" FOREIGN KEY("assessment_id") REFERENCES "Assessment Scheme"("id");
ALTER TABLE
    "Class" ADD CONSTRAINT "class_user_id_foreign" FOREIGN KEY("user_id") REFERENCES "User"("user_id");
ALTER TABLE
    "Unit" ADD CONSTRAINT "unit_delivery_id_foreign" FOREIGN KEY("delivery_id") REFERENCES "Delivery Type"("delivery_id");